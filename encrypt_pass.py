import sys
from random import shuffle
from string import ascii_lowercase,ascii_uppercase

def change_pass(password,enc1,enc2):
	lpas = list(password)
	for i in range(len(lpas)):
		let = lpas[i]
		if let in ascii_lowercase:
			ind = ascii_lowercase.index(let)
			newlet = enc1[ind]
		elif let in ascii_uppercase:
			ind = ascii_uppercase.index(let)
			newlet = enc2[ind]
		else:
			continue
		lpas[i] = newlet
	return "".join(lpas)

code1 = ascii_lowercase
code2 = ascii_uppercase

p = open("pass.txt")
password = p.read()
p.close()

f = open("encryptions.txt","w+")

count = 0
newpass = password
while count < 5:
	lc1 = list(code1)
	lc2 = list(code2)
	shuffle(lc1)
	shuffle(lc2)
	code1 =  "".join(lc1)
	code2 =  "".join(lc2)

	newpass = change_pass(newpass,code1,code2)
	print(newpass)
	f.write(code1+code2+"\n")
	count+=1

f.write(newpass)
f.close()
